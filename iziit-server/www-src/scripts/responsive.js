import bootbox from './bootbox-loader';

let isDesktop, isFirefox, isChrome, isMac;

function detectDevice() {
	const macPlatforms = ['Mac68K', 'MacPPC', 'MacIntel', 'iPhone', 'iPod', 'iPad'];

	isDesktop = !('ontouchstart' in window);
	isFirefox = navigator.userAgent.match(/Firefox\/.+/) && !navigator.userAgent.match(/Seamonkey\/.+/) ? true : false;
	isChrome = navigator.userAgent.match(/Chrome\/.+/) && !navigator.userAgent.match(/Chromium\/.+/) ? true : false;
	isMac = ~macPlatforms.indexOf(window.navigator.platform) ? true : false;

	// if error
	if (isFirefox && isChrome) {
		firefox = false;
		chrome = false;
	}
}

function buildDownloadButton() {
	const downloadButton = document.getElementById('download');

	if (isDesktop && isFirefox) {

		downloadButton.setAttribute('href', 'https://addons.mozilla.org/fr/firefox/addon/iziit/');

	} else if (isDesktop && isChrome) {

		downloadButton.setAttribute('href', 'https://chrome.google.com/webstore/detail/iziit/djpkafbgnfeinacocpaipkigbnmccjea');

	} else {

		downloadButton.addEventListener('click', () => {

			downloadButton.classList.add('unknown-browser');
			downloadButton.querySelector('p').innerText = 'incompatible';

			if (!isDesktop){
				bootbox.alert('Iziit n\'est pour l\'instant disponible que sur PC et Mac.');
			} else {
				bootbox.alert(
					'Nous n\'arrivons pas à identifier votre navigateur.</br>'
					+ 'Cliquez <a href="https://addons.mozilla.org/fr/firefox/addon/iziit/" target="_blank" class="animated-link animated-link-green">ici</a> pour installer Iziit sur Firefox.</br>'
					+ 'Cliquez <a href="https://chrome.google.com/webstore/detail/iziit/djpkafbgnfeinacocpaipkigbnmccjea?utm_source=inline-install-disabled" target="_blank" class="animated-link animated-link-green">ici</a> pour installer Iziit sur Chrome.</br>'
					+ 'Si vous avez des difficultés, <a href="/contact" class="animated-link animated-link-green">contactez-nous</a>.'
				);
			}

			return false;
		});
	}
}

function buildVideo() {
	const fileName = isMac ? 'mac-demo' : 'demo';
	const video = document.querySelector('video');

	for (const format of ['webm', 'mp4']){
		const element = document.createElement('source');
		element.setAttribute('type', `video/${format}`);
		element.setAttribute('src', `https://static.iziit.org/videos/${fileName}.${format}`);
		video.appendChild(element);
	}
}

function handleResize() {
	let viewWidth;

	function onResize() {

		const currentViewWidth = document.documentElement.clientWidth || document.body.clientWidth;

		if (viewWidth === currentViewWidth) {
			return;
		} else {
			viewWidth = currentViewWidth;
		}

		const ribbon = document.getElementById('ribbon');
		const logos = document.getElementById('logos');

		if (viewWidth > 700) {

			let ribbonHeight = 800 - viewWidth / 2;
			if (ribbonHeight < 300) ribbonHeight = 300;

			const angle = -0.17;
			const logosLeftMargin = Math.tan(Math.abs(angle)) * ribbonHeight / 2 + 10;

			ribbon.style.height = ribbonHeight + 'px';
			logos.style.width = '';
			logos.style.marginLeft = logosLeftMargin + 'px';

		} else {

			ribbon.style.height = '';
			logos.style.width = '100%';
			logos.style.marginLeft = '';
		}
	}

	window.addEventListener('resize', onResize);
	onResize();
}

function main() {
	detectDevice();
	buildDownloadButton();
	buildVideo();
	handleResize();
}

main();

