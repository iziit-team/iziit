import bootbox from 'bootbox';
import * as bootstrap from 'bootstrap';

const cssUrl = `https://stackpath.bootstrapcdn.com/bootstrap/${bootstrap.Modal.VERSION}/css/bootstrap.min.css`;

function linkPresent() {
	return document.querySelector(`link[href="${cssUrl}"]`) ? true : false;
}

function buildLink() {
	const link = document.createElement('link');
	link.rel = 'stylesheet';
	link.href = cssUrl;
	link.crossOrigin = 'anonymous';
	return link;
}

function insertLink(link) {
	const head = document.head;
	const firstCssLink = document.querySelector('link[rel=stylesheet]');
	if (firstCssLink) {
		head.insertBefore(link, firstCssLink);
	} else {
		head.appendChild(link);
	}
}

if (!linkPresent()) {
	insertLink(buildLink());
}

export default bootbox;
