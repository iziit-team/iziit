// Copyright 2019 Clément Saccoccio

/*
This file is part of Iziit Server.

Iziit Server is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Iziit Server is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Iziit Server.  If not, see <https://www.gnu.org/licenses/>.
*/

import { Application } from 'express';
import { readFileSync } from 'fs';
import * as http from 'http';
import * as https from 'https';
import { serverConfig } from './Configuration';

export default class Server {

	private static httpPort = serverConfig?.http?.port ?? 80;
	private static httpsPort = serverConfig?.https?.port ?? 443;

	public static runApp(app: Application) {
		if (serverConfig.https) {
			Server.runAppOnHttps(app);
			Server.redirectHttpToHttps();
			console.log(`server started on port ${Server.httpsPort} and ${Server.httpPort} (redirect)`);
		} else {
			Server.runAppOntHttp(app);
			console.log(`server started on port ${Server.httpPort}`);
		}
	}

	private static runAppOnHttps(app: Application) {
		const options = {
			cert: readFileSync(serverConfig.https.certificateLocation),
			key: readFileSync(serverConfig.https.secretKeyLocation),
		};
		https.createServer(options, app).listen(Server.httpsPort);
	}

	private static runAppOntHttp(app: Application) {
		app.listen(Server.httpPort);
	}

	private static redirectHttpToHttps() {
		const hostnamePattern = /^(.+?)(?::\d+)?$/;
		http.createServer((req, res) => {
			if (req.headers.host.match(hostnamePattern)) {
				const url = `https://${RegExp.$1}:${Server.httpsPort}${req.url}`;
				res.writeHead(302, { Location: url });
			} else {
				res.writeHead(400);
			}
			res.end();
		}).listen(Server.httpPort);
	}
}
