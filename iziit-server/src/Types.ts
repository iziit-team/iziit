// Copyright 2019 Clément Saccoccio

/*
This file is part of Iziit Server.

Iziit Server is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Iziit Server is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Iziit Server.  If not, see <https://www.gnu.org/licenses/>.
*/

export interface ServerConfig {
	http?: {
		port?: number,
	};
	https?: {
		port?: number,
		secretKeyLocation: string,
		certificateLocation: string,
	};
	reCaptchaSecretKey?: string;
	addonBackend?: AddonBackendConfig;
}

export interface AddonBackendConfig {
	maxProductsPerRequest: number;
	databases: DatabaseConfig;
}

export interface DatabaseConfig {
	updateTime: string;
	nbOfDaysBeforeDeletionWhenUnused: number;
	nbOfDaysBeforeUpdating: number;
	credentials?: MongodbCredentials;
	options?: AnyObject;
}

export interface MongodbCredentials {
	login: string;
	password: string;
	authSource: string;
}

export enum Brand {
	Auchan = 'auchan',
	Chronodrive = 'chronodrive',
	Intermarche = 'intermarche',
	Leclerc = 'leclerc',
	Udrive= 'udrive',
}

export interface Composition {
	'palm-oil': boolean;
	'gluten': boolean;
	'crustaceans': boolean;
	'eggs': boolean;
	'fishes': boolean;
	'peanut': boolean;
	'soy': boolean;
	'milk': boolean;
	'nuts': boolean;
	'celery': boolean;
	'mustard': boolean;
	'sesame-seeds': boolean;
	'sulfure-dioxyde-and-sulphites': boolean;
	'lupine': boolean;
	'moluscs': boolean;
}

export type IngredientsList = [
	'palm-oil'?,
	'gluten'?,
	'crustaceans'?,
	'eggs'?,
	'fishes'?,
	'peanut'?,
	'soy'?,
	'milk'?,
	'nuts'?,
	'celery'?,
	'mustard'?,
	'sesame-seeds'?,
	'sulfure-dioxyde-and-sulphites'?,
	'lupine'?,
	'moluscs'?,
];

export type Ingredient = keyof Composition;

export interface PayloadBase {
	ingredientsToAvoid: IngredientsList;
}

export interface AnyPayload extends PayloadBase, AnyObject {}

export interface AnyObject {
	[key: string]: any;
}

export interface ReCaptchaApiResponse {
	success: boolean;
	challenge_ts: string;   // timestamp of the challenge load (ISO format yyyy-MM-dd'T'HH:mm:ssZZ)
	hostname: string;       // the hostname of the site where the reCAPTCHA was solved
	'error-codes'?: string[];
}

// generic functions
export type Runnable = () => void;
export type SimpleFunction<T, R> = (t: T) => R;
export type BiFunction<T, U, R> = (t: T, u: U) => R;
export type Consumer<T> = (t: T) => void;
export type BiConsumer<T, U> = (t: T, u: U) => void;
export type Supplier<R> = () => R;
