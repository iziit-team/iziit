// Copyright 2019 Clément Saccoccio

/*
This file is part of Iziit Server.

Iziit Server is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Iziit Server is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Iziit Server.  If not, see <https://www.gnu.org/licenses/>.
*/

import AuchanGroupDbHandler from './AuchanGroupDbHandler';

export default class ChronodriveDbHandler extends AuchanGroupDbHandler {

	private static productPageUrlParser: RegExp = /https:\/\/www\.chronodrive\.com\/(.+)-([A-Z\d]+)/;
	private static saleDescAndOriginMentionFilter: RegExp = /^(Dénomination\s+légale\s+de\s+vente|Mention\s+d'origine\s+géographique)/;

	protected getProductPageUrlParser(): RegExp {
		return ChronodriveDbHandler.productPageUrlParser;
	}

	protected buildProductPageUrl(productId: number, productName: string): string {
		return `https://www.chronodrive.com/${productName}-${productId}`;
	}

	protected buildCookie(shopId: number): string {
		return `chronoShop="shopId=${shopId}"`;
	}

	protected getProductCompositionListFromHtmlDocument(document: Document): string {
		let composition: string = '';

		const column2Elements = document.getElementsByClassName('column2');
		let title: any;

		for (const column2Element of column2Elements) {

			title = column2Element.getElementsByTagName('h2')[0];
			if (!title || title.textContent !== 'Ingrédients :') continue;

			const paragraphes = column2Element.getElementsByTagName('p');

			for (const paragraphe of paragraphes) {
				if (paragraphe.textContent.match(ChronodriveDbHandler.saleDescAndOriginMentionFilter)) {
					break;
				}
				composition += paragraphe.textContent + ' ';
			}

			break;
		}

		const cells = document.querySelectorAll('table.allergenes td:not(:first-child)');

		for (const cell of cells) {
			const content = cell.textContent;
			if (content !== '') composition += content + ' ';
		}

		return composition;
	}
}
