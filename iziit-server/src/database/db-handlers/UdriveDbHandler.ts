/*
This file is part of Iziit Server.

Iziit Server is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Iziit Server is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Iziit Server.  If not, see <https://www.gnu.org/licenses/>.
*/

// import AuchanGroupDbHandler from './AuchanGroupDbHandler';

// export default class UdriveDbHandler extends AuchanGroupDbHandler {

// 	private static productPageUrlParser: RegExp = /https:\/\/www\.coursesu\.com\/p\/(.+)\/([A-Z\d]+)\.html/;
// 	private static nutritionnalInfosFilter: RegExp = /^Autres\s+infos\s+nutritionnelles:.*?(?:Ingrédients:(.*))?$/;

// 	protected getProductPageUrlParser() {
// 		return UdriveDbHandler.productPageUrlParser;
// 	}

// 	protected buildProductPageUrl(productId: number, productName: string): string {
// 		return `https://www.coursesu.com/p/${productName}/${productId}.html`;
// 	}

// 	protected buildCookie(shopId: number): string {
// 		return `storeId=${shopId}`;
// 	}

// 	protected getProductCompositionListFromHtmlDocument(document: Document): string {

// 		let composition: string = '';

// 		const infoContainers = document.getElementsByClassName('pdp-bottom-infos__container');
// 		let title: any;
// 		let content: any;

// 		for (const infoContainer of infoContainers) {

// 			title = infoContainer.getElementsByTagName('span')[0];
// 			if (!title) continue;
// 			title = title.textContent;

// 			if (!title.match(/composition/i)) continue;

// 			content = infoContainer.getElementsByClassName('pdp-bottom-infos__content')[0];
// 			if (!content) break;
// 			content = content.textContent;

// 			const match: RegExpMatchArray = content.match(UdriveDbHandler.nutritionnalInfosFilter);

// 			if (match) composition += match[1] || '';
// 			else composition += content;

// 			break;
// 		}

// 		return composition;
// 	}
// }

import { JSDOM } from 'jsdom';
import * as request from 'superagent';
import { AnyObject, Composition, Supplier } from '../../Types';
import { Response } from 'superagent';
import { sendRequest } from '../../Utils';
import { MongodbProductDocument, MongodbShopDocument } from '../Documents';
import DbHandler from './DbHandler';

interface Product {
	_id: number;
	name: string;
}

interface ParsedUdriveUrl {
	product: Product;
}

export default class UdriveDbHandler extends DbHandler {

	private static urlParser: RegExp = /^https:\/\/www\.coursesu\.com\/p\/(.+)\/([A-Z\d]+)\.html$/;
	private static nutritionnalInfosFilter: RegExp = /.*(\n *)|(.*Ingrédients :)/;

	private static parseProductPageUrl(productPageUrl: string): ParsedUdriveUrl {
		const match = productPageUrl.match(UdriveDbHandler.urlParser);

		if (!match || typeof match[0] === undefined || typeof match[0] === undefined) {
			throw new Error(`unexpected url architecture: ${productPageUrl}`);
		}
		return {
			product: {
				_id: parseInt(match[2], 10),
				name: match[1],
			}
		};
	}

	public async init(): Promise<void> {
		return Promise.resolve();
	}

	public async getProductComposition(shopId: number, productPageUrl: string): Promise<Composition> {
		const infos: ParsedUdriveUrl = UdriveDbHandler.parseProductPageUrl(productPageUrl);

		return await super.getProductComposition2(shopId, infos.product._id, infos.product.name, () => this.getProductCompositionOnline(shopId, productPageUrl));
	}

	protected async buildOnlineProductCompositionSupplier(shopId: number, document: MongodbProductDocument): Promise<Supplier<Promise<Composition>>> {

		return () => this.getProductCompositionOnline(shopId, this.buildProductPageUrl(document._id, document.name));
	}

	protected buildProductPageUrl(_id: number, name: string): string {
		return `https://www.coursesu.com/p/${name}/${_id}.html`;
	}

	protected buildCookie(shopId: number): string {
		return `storeId=${shopId}`;
	}

	protected getProductCompositionListFromHtmlDocument(document: Document): string {
		let composition= '';

		const infoContainers = document.getElementsByClassName('main-information-content');
		let title: any;
		let content: any;

		for (const infoContainer of infoContainers) {

			title = infoContainer.getElementsByTagName('span')[0];
			if (!title) continue;
			title = title.textContent;

			// if (!title.match(/composition/i)) continue;
			let compoContainer = document.getElementById("panel1");
			if(compoContainer.getElementsByTagName('p')[1]){
				content = infoContainer.getElementsByTagName('p')[1];
			}
			else {
				content = infoContainer.getElementsByTagName('p')[0];
			}
			if (!content) break;
			content = content.textContent;
			composition += content.toLowerCase();
			break;
		}
		return composition;
	}

	private async getProductCompositionOnline(shopId: number, productPageUrl: string): Promise<Composition> {
		let res: Response;

		try {
			res = await sendRequest(request.get(productPageUrl).set('Cookie', this.buildCookie(shopId)));
		} catch (err) {
			console.warn(`${err.status}: ${productPageUrl}, shopId=${shopId}`);
			return undefined;
		}

		const document: Document = new JSDOM(res.text).window.document;
		let composition: string = this.getProductCompositionListFromHtmlDocument(document);
		let match = composition.match(/[A-Za-z].*\..*/);
		if(match != null){
			composition=match[0];
		}
		if (composition === '') return undefined;
		else return DbHandler.buildCompositionFromIngredientsList(composition);
	}

}