/*
This file is part of Iziit Server.

Iziit Server is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Iziit Server is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Iziit Server.  If not, see <https://www.gnu.org/licenses/>.
*/

// import { Brand } from '../Types';
// import AuchanGroupApi from './AuchanGroupApi';

// export default class UdriveApi extends AuchanGroupApi {

// 	public constructor() {
// 		super(Brand.Udrive);
// 	}

// 	protected getUrlPattern(): RegExp {
// 		return /^https:\/\/(www\.)?coursesu\.com\/[\w-\/]+\/[A-Z0-9]+\.html$/;
// 	}
// }

import { Response } from 'express';
import Skeletons from 'skeletons';
import { addonBackendConfig } from '../Configuration';
import UdriveDbHandler from '../database/db-handlers/UdriveDbHandler';
import { isIngredientList } from '../SupervisedIngredients';
import { Brand, PayloadBase, AnyPayload } from '../Types';
import Api from './Api';

interface Product {
	id: number;
	productPage: string;
	name: string;
}

interface UdrivePayload extends PayloadBase {
	protocolVersion: number;
	shopId: number;
	products: Product[];
}

interface ResponsePayload {
	products: {
		[key: number]: boolean,
	};
}

export default class UdriveApi extends Api<UdriveDbHandler, UdrivePayload> {

    public constructor() {
        super(Brand.Udrive);
	}

    protected async handleClientPayload(payload: UdrivePayload, res: Response) {
        const promises = [];
		const responsePayload: ResponsePayload = { products: {} };
		for (const product of payload.products) {
			let match = product.toString().match(/^https:\/\/www\.coursesu\.com\/p\/(.+)\/([A-Z\d]+)\.html$/);
			if(match != undefined){
				let id = parseInt(match[2], 10);
				promises.push(
					this.dbHandler.getProductComposition(payload.shopId, product.toString())
					.then((composition) => {
						if (!composition) return;
						responsePayload.products[id] = !Api.doesProductContainsIngredientToAvoid(payload.ingredientsToAvoid, composition);
					}).catch(console.error));
			}
		}
		await Promise.all(promises);
		res.setHeader ('Content-Type', 'application/json');
		res.status(200).send(JSON.stringify(responsePayload.products));
	}

    protected getPayloadValidator(): Skeletons {
		return new Skeletons({
			ingredientsToAvoid: Skeletons.Array({
				validator: isIngredientList,
			}),
			products: Skeletons.Array({
				item: Skeletons.String({
					match: this.getUrlPattern(),
				}),
				maxLength: addonBackendConfig.maxProductsPerRequest,
				minLength: 1,
			}),
			protocolVersion: Skeletons.Number({
				strictEquals: 1,
			}),
			shopId: Skeletons.Number({
				min: 0,
			}),
		});
	}

	
	
	protected getUrlPattern(): RegExp {
		return /^https:\/\/(www\.)?coursesu\.com\/[\w-=&?!:\/]+(.html)?$/;
	}
}
