// Copyright 2019 Clément Saccoccio

/*
This file is part of Iziit Server.

Iziit Server is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Iziit Server is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Iziit Server.  If not, see <https://www.gnu.org/licenses/>.
*/

import { Response } from 'express';
import Skeletons from 'skeletons';
import { addonBackendConfig } from '../Configuration';
import AuchanDbHandler from '../database/db-handlers/AuchanDbHandler';
import { isIngredientList } from '../SupervisedIngredients';
import { AnyPayload, Brand } from '../Types';
import Api from './Api';

interface AuchanGroupPayload extends AnyPayload {
	products: string[];
	shopId: number;
	protocolVersion: number;
}

interface ResponsePayload {
	[key: string]: boolean;
}

export default abstract class AuchanGroupApi extends Api<AuchanDbHandler, AuchanGroupPayload> {

	public constructor(brand: Brand) {
		super(brand);
	}

	protected async handleClientPayload(payload: AuchanGroupPayload, res: Response) {
		const promises = [];
		const responsePayload: ResponsePayload = {};
		for (const product of payload.products) {
			promises.push(
				this.dbHandler.getProductComposition(payload.shopId, product)
				.then((composition) => {
					if (!composition) return;
					responsePayload[product.match(/[^-]*$/)[0]] = !Api.doesProductContainsIngredientToAvoid(payload.ingredientsToAvoid, composition);
				}).catch(console.error));
		}
		await Promise.all(promises);
		res.setHeader ('Content-Type', 'application/json');
		res.status(200).send(JSON.stringify(responsePayload));
	}

	protected getPayloadValidator(): Skeletons {
		return new Skeletons({
			ingredientsToAvoid: Skeletons.Array({
				validator: isIngredientList,
			}),
			products: Skeletons.Array({
				item: Skeletons.String({
					match: this.getUrlPattern(),
				}),
				maxLength: addonBackendConfig.maxProductsPerRequest,
				minLength: 1,
			}),
			protocolVersion: Skeletons.Number({
				strictEquals: 1,
			}),
			shopId: Skeletons.Number({
				min: 0,
			}),
		});
	}

	protected abstract getUrlPattern(): RegExp;
}
