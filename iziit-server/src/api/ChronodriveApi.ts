// Copyright 2019 Clément Saccoccio

/*
This file is part of Iziit Server.

Iziit Server is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Iziit Server is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Iziit Server.  If not, see <https://www.gnu.org/licenses/>.
*/

import { Brand } from '../Types';
import AuchanGroupApi from './AuchanGroupApi';

export default class ChronodriveApi extends AuchanGroupApi {

	public constructor() {
		super(Brand.Chronodrive);
	}

	protected getUrlPattern(): RegExp {
		return /^https:\/\/(www\.)?chronodrive\.com\/[\w-\/]+-[A-Z0-9]+$/;
	}
}
