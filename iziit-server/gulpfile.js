'use strict';
 
var gulp = require('gulp');
var sass = require('gulp-sass');
 
sass.compiler = require('node-sass');
 
gulp.task('sass', function () {
  return gulp.src('./www-src/styles/sass/**/*.scss')
    .pipe(sass.sync().on('error', sass.logError))
    .pipe(gulp.dest('./www-src/styles/css'));
});
 
gulp.task('sass:watch', function () {
  gulp.watch('./www-src/styles/sass/**/*.scss', ['sass']);
});