# LAUNCH

```
docker-compose up
npm run server
```

# Generate a private key and a certificate in order to use HTTPS. This command generates them for the domain name "localhost":
```
sudo su -c 'openssl req -x509 -out localhost.crt -keyout localhost.key \
    -newkey rsa:2048 -nodes -sha256 \
    -subj '/CN=localhost' -extensions EXT -config <( \
     printf "[dn]\nCN=localhost\n[req]\ndistinguished_name = dn\n[EXT]\nsubjectAltName=DNS:localhost\nkeyUsage=digitalSignature\nextendedKeyUsage=serverAuth")'
```

# Edit confg.json
```
{
	"http": {
		"port": 8080
	},
	"https": {
		"port": 4443,
		"secretKeyLocation": "<absolute path to localhost.key>",
		"certificateLocation": "<absolute path to localhost.crt>"
	},
	"addonBackend": {
		"maxProductsPerRequest": 1000,
		"databases": {
			"updateTime": "02:00",
			"nbOfDaysBeforeDeletionWhenUnused": 3,
			"nbOfDaysBeforeUpdating": 2,
			"credentials": {
				"login": "iziit1",
				"password": "Q8g3vJsW2",
				"authSource": "admin"
            }
        }
	}
}
```

# Commands
## in iziit-server
```
./scripts/extension
```
## in iziit-client
```
npm run dev-firefox || npm run dev-chrome
```