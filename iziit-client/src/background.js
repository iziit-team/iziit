/*
Copyright 2018, 2019, 2020 Clément Saccoccio
Licenced under GPL-3.0-or-later

This file is part of Iziit.
Iziit is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
Iziit is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with Iziit. If not, see <https://www.gnu.org/licenses/>.

You can contact us at contact@iziit.org
*/

// use the background page to transmit XHR to the server in order to avoid CORB (Cross Origin Read Blocking)
chrome.runtime.onMessage.addListener(
	function(request, _sender, sendResponse) {
		const xhr = new XMLHttpRequest();
		xhr.open('POST', request.url);
		xhr.setRequestHeader('Content-Type', 'application/json; charset=utf-8');
		xhr.addEventListener('readystatechange', () => {
			if (xhr.readyState === XMLHttpRequest.DONE) {
				sendResponse({
					statusCode: xhr.status,
					body: xhr.response
				});
			}
		});
		xhr.send(request.data);
		return true;
	}
);